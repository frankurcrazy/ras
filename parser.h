#ifndef __PARSER_H__
#define __PARSER_H__

#define LINE_BUFSIZE 15001
#define CMD_BUFSIZE 255
#define STDERR_PIPE 2
#define STDOUT_PIPE 1


struct cmdline { 
    int id; /* id of the line */
    char input[LINE_BUFSIZE];
    int cmd_count;
    struct cmd *cmds;
    int filefd;
    char* filename;
    int stdout_target;
    int stderr_target;
};

struct cmd {
    char *cmd;
    char **argv;
    struct cmd* next_cmd;
};

struct pipe {
    int id;
    int pipefd[2];
    struct pipe* next_pipe;
};

char* normalize(char*);
int parse(struct cmdline*, struct pipe**, int);
int get_pipe_id(char*, int*, int);
int add_pipe(struct pipe**, int);
int del_pipe(struct pipe**, int);
struct cmd* parse_cmd(char*,int*);
int fork_execute(struct cmdline**, struct pipe**);
int fork_pipes(struct cmd*, int, int, int);
int get_file_redirection(char*);
char* get_filename(char *input);

#endif

