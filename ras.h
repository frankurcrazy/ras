#ifndef __RAS_H__
#define __RAS_H__
#include "parser.h"
void banner();
int prompt(struct cmdline **, struct pipe**);
int main_loop();
int fork_execute(struct cmdline**, struct pipe**);

#endif
