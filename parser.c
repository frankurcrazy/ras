#include "parser.h"
#include "misc.h"
#include <stdio.h>
#include <stdlib.h> 
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

int del_pipe(struct pipe** pipe_list, int id) {
    struct pipe* pipe_iter = NULL;
    struct pipe* pipe_iter_pre = NULL;

    pipe_iter = pipe_iter_pre = *pipe_list;
    while (pipe_iter) {
        if (pipe_iter->id == id) {
            if (*pipe_list == pipe_iter) {
                /* this is the head */
                *pipe_list = pipe_iter -> next_pipe;
                free(pipe_iter);
                return 0;
            } else {
                /* this is in between this link list */
                pipe_iter_pre->next_pipe = pipe_iter->next_pipe;
                free(pipe_iter);
                return 0;
            }
        } else {
            pipe_iter_pre = pipe_iter;
            pipe_iter = pipe_iter->next_pipe;
        }
    }

    return -1;
}
int add_pipe(struct pipe** pipe_list, int id) {
    struct pipe* pipe_iter = NULL;
    struct pipe* new_pipe = NULL;

    /* traverse through the pipe list and find existing pipe */
    pipe_iter = *pipe_list;
    while ( pipe_iter && pipe_iter->id!=id ) {
        pipe_iter = pipe_iter->next_pipe;
    }

    if (pipe_iter) {
        return 0;
    }

    /* empty pipe list */
    new_pipe = (struct pipe*) calloc(sizeof(struct pipe), 1);
    if (!new_pipe) goto onerror;
    new_pipe->id = id;
    if (pipe(new_pipe->pipefd) < 0) {
        goto onerror;
    }

    new_pipe->next_pipe = *pipe_list;
    *pipe_list = new_pipe;

    return 0;

    onerror:
    if (new_pipe->pipefd[0] > 0) close(new_pipe->pipefd[0]);
    if (new_pipe->pipefd[1] > 0) close(new_pipe->pipefd[1]);
    if (new_pipe > 0) {
        free(new_pipe);
    }
    return -1;
}

int parse(struct cmdline* org, struct pipe** p_pipe, int line_number) {
    int type = 0;
    int pipe = 0;
    int filefd = 0;
    char *filename;

    /* normalize */
    normalize(org->input); 

    /* get file redirection fd */
    /*
    if ((filefd = get_file_redirection(org->input))>0) {
        org->filefd = filefd;
    */
    if ((filename = get_filename(org->input)) != NULL) {
        //org->filefd = filefd;
        org->filename = strdup(filename);
    } else {
        /* get pipe id */
        while ( (pipe = get_pipe_id(org->input, &type, line_number)) != 0) {
            if (pipe > 0) {
                if (add_pipe(p_pipe, pipe) != 0) {
                    return -1;
                }

                if (type == STDERR_PIPE) {
                    org->stderr_target = pipe;
                } else if (type == STDOUT_PIPE) {
                    org->stdout_target = pipe;
                }
            }
        }
    }

    org->cmds = parse_cmd(org->input, &(org->cmd_count));

    return 0;
}

char* normalize(char* org) {
    register int i = 0;
    int len = 0;
    char *trimmed;

    len = strlen(org);
    for (i=0; i<len; ++i) {
        if (org[i] == '\r' || org[i] == '\n') {
            org[i] = ' ';
        }
    }

    trimmed = trim(org);
    strncpy(org, trimmed, strlen(trimmed)+1);

    return org;
}

int get_file_redirection(char* input) {
    int len = 0;
    int fd = 0;
    char* filename = NULL;
    register int i = 0;

    len = strlen(input);

    for (i = len; i>=0; --i) {
        if (input[i] == '>') {
            input[i] = '\0';
            filename = trim(input+i+1); 
        
            fd = open(filename, O_WRONLY|O_TRUNC|O_CREAT , 0644);
            if (fd < 0) {
                perror("open()");
                return -1;
            } else {
                return fd;
            }
        }
    }

    return 0;
}

char* get_filename(char *input) {
    int len = 0;
    int fd = 0;
    char* filename = NULL;
    register int i = 0;

    len = strlen(input);

    for (i = len; i>=0; --i) {
        if (input[i] == '>') {
            input[i] = '\0';
            filename = trim(input+i+1); 
        }
    }

    return filename;

}

/*
    get the target line id of the line if there's any
    else return 0
*/
int get_pipe_id(char* input, int* type, int line_number) {
    int len = 0;
    int pipe = 0;
    register int i = 0;

    len = strlen(input);

    for (i=len-1; i>=0; --i) {
        if (input[i] == '|' || input[i] == '!') {
            break;
        }
    }

    if (!(input[i+1] >= '0' && input[i+1] <= '9')) {
        return 0;
    }

    if ( input[i] == '|' ) {
        *type = 1;
    } else if (input[i] == '!') {
        *type = 2;
    }

    pipe = atoi(input+i+1);
    input[i--] = '\0'; 

    /* stripe the spaces in the back */
    for (; input[i] == ' ' && i>=0; --i) {
        input[i] = '\0';
    }

    return pipe+line_number;
}

struct cmd* parse_cmd(char* input, int *count) {
    const char* delim_pipe = "|";
    const char* delim_space = " \t";
    char *saveptr_pipe;
    char *saveptr_space;
    char *pipe, *argv;
    struct cmd* new_cmd = NULL;
    struct cmd* head = NULL;
    struct cmd* tail = NULL;
    int argc = 0;

    *count = 0;

    pipe = strtok_r(input, delim_pipe, &saveptr_pipe);
    while( pipe ) {
        argc = 0;
        ++(*count);
        new_cmd = (struct cmd*) calloc(sizeof(struct cmd), 1);
        if (!new_cmd) goto onerror;

        new_cmd->argv = (char**) calloc(sizeof(char**), 2);
        argv = strtok_r(pipe, delim_space, &saveptr_space);
        if (!argv) goto onerror;
        else {
            new_cmd->argv[argc++] = argv;
            new_cmd->cmd = argv;
        }

        while ( (argv = strtok_r(NULL, delim_space, &saveptr_space)) ) {
            new_cmd->argv = (char**)realloc((void*)new_cmd->argv, sizeof(char**)*(argc+2));
            new_cmd->argv[argc+1] = NULL;
            new_cmd->argv[argc++] = argv;
        }

        new_cmd->cmd = new_cmd->argv[0];

        if (head != NULL) {
            tail->next_cmd=new_cmd;
            tail = new_cmd;
        } else {
            head = tail = new_cmd;
        }

        pipe = strtok_r(NULL, delim_pipe, &saveptr_pipe);
    }

    return head;

    onerror:
    while (head) {
        new_cmd = head->next_cmd;
        free(head);
        head = new_cmd;
    }
    *count = 0;
    return NULL;
}
