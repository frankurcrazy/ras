#include "conn.h"
#include "ras.h"
#include "parser.h"
#include <sys/select.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <fcntl.h>

#define BACKLOG 10

int main() {
    int msock = 0;
    int ssock = 0;
    int select_ret = 0;
    int ret = 0;
    int len = 0;
    FILE *client;
    pid_t pid = 0;
    fd_set rfds, _rfds;
    struct sockaddr_in saddr;

    //signal(SIGCHLD, SIG_IGN);
    
    msock = passiveTCP("0.0.0.0", 8258, BACKLOG);
    if (msock < 0) goto onerror;

    FD_ZERO(&_rfds);
    FD_SET(msock, &_rfds);
    bzero(&saddr, sizeof(saddr));

    rfds = _rfds;
    while ( (select_ret = select(msock+1, &rfds, NULL, NULL, NULL)) ) {
        fflush(stdout);
        pid = fork();
        if (pid > 0) {
            continue;
        } else if (pid == 0) {
            setenv("PATH", "bin:.", 1);
            chdir("./ras");
            len = sizeof(struct sockaddr_in);
            ssock = accept(msock, (struct sockaddr*) &saddr, &len);
            if (ssock < 0) {
                perror("accept()");
                return EXIT_FAILURE;
            }
            fprintf(stderr, "Client connected from %s:%d\n", \
                inet_ntoa(saddr.sin_addr), ntohs(saddr.sin_port));

            /* duplicate it to stdin, stdout, stderr */
            dup2(ssock, STDIN_FILENO);
            dup2(ssock, STDOUT_FILENO);
            dup2(ssock, STDERR_FILENO);
            close(ssock);

            /* disable buffer on output */
            setbuf(stdout, NULL);

            banner();
            main_loop();

            return EXIT_SUCCESS;
        } else {
            perror("fork()");
        }

        rfds = _rfds;
    }

    return EXIT_SUCCESS;

    onerror:
    shutdown(ssock, SHUT_RDWR);
    shutdown(msock, SHUT_RDWR);
    if (ssock > 0) close(ssock);
    if (msock > 0) close(msock);

    return EXIT_FAILURE;
}

int main_loop() {
    int ret;
    struct cmdline *cmd = NULL;
    struct pipe* pipe = NULL;

    while (1) {
        ret = prompt(&cmd, &pipe);
        if (ret != EXIT_SUCCESS) return -1;

        fork_execute(&cmd, &pipe);
    }
}

void banner() {
    printf(
        "****************************************\n" \
        "** Welcome to the information server. **\n" \
        "****************************************\n" \
    );

    return;
}

int prompt(struct cmdline** cmd, struct pipe** pipe) {
    char *ret; 
    struct cmdline *new_cmdline = NULL;
    static unsigned int line_number = 0;

    printf("%% ", line_number);
    
    new_cmdline = (struct cmdline*) calloc(sizeof(struct cmdline), 1);
    new_cmdline->id  = ++line_number;

    if (!new_cmdline) { 
        goto onerror;
    }

    if ( (ret = fgets(new_cmdline->input, LINE_BUFSIZE-1, stdin)) <= 0) {
        if (!feof(stdin)) {
            goto onerror;
        }
    }
    
    if (parse(new_cmdline, pipe, line_number) < 0) goto onerror;
    *cmd = new_cmdline;
    return 0;

    onerror:
    // free all the memory
    if (new_cmdline) free(new_cmdline);
    return -1;
}

int fork_execute(struct cmdline** cmd, struct pipe** pipes) {
    int in = 0, out = 0, err = 0;
    int i;
    struct pipe* p;
    int *pipefd = NULL;
    int cmd_count = 0;
    struct cmd* c;
    int close_out = 1, close_err = 1;
    int status =0;
    int line_id = 0;
    char resp[10240];
    char tempfile[255];

    cmd_count = (*cmd)->cmd_count;

    pipefd = (int*) calloc(sizeof(int), (cmd_count-1)*2);
    if (!pipefd) goto onerror;

    /* create n-1 pipes */
    //for (i=0; i<cmd_count-1; ++i) {
    //    if (pipe(pipefd+2*i) < 0) {
    //        perror("pipe()"); 
    //    }
    //}

    c = (*cmd)->cmds;
    line_id = (*cmd)->id;
    for (i=0; c && i<cmd_count; ++i) {
        if (i!=cmd_count-1 || (cmd_count > 1 && i==0)) {
            pipe(pipefd+2*i);
        }

        if (i>0) {
            in = *(pipefd+2*(i-1));
        } else {
            /* first command */
            /* search for input pipe */
            p = *pipes;
            while ( p ) {
                if (p->id == line_id) {
                    close(p->pipefd[1]); // close the writing end
                    in = p->pipefd[0];
                    break;
                }
                p = p->next_pipe;
            }
            if (!in) in = STDIN_FILENO;
        }

        if (i==cmd_count-1) {
            /* last command */
            #if 0
            if ( (*cmd)->filefd > 0 ) {
                /* file redirection */
                out = (*cmd)->filefd;
                close_out = 1;
            #endif
            if ( (*cmd)->filename ) {
                snprintf(tempfile, 255, "/tmp/.temp-ras%d", getpid());
                out = open(tempfile, O_WRONLY|O_TRUNC|O_CREAT, 0660);
                if (out < 0) {
                    perror("open()");
                    goto onerror;
                }
                close_out = 1;
            } else {
                if ( (*cmd)->stdout_target != 0 ) {
                    p = *pipes;
                    while (p) {
                        if (p->id == (*cmd)->stdout_target) {
                            out = p->pipefd[1];
                            close_out = 0;
                            break;
                        }
                        /* multi-line pipe */
                        p = p->next_pipe;
                    }
                    if (!p) return -1;
                } else {
                    out = STDOUT_FILENO;
                    close_out = 1;
                }
            }

            if ( (*cmd)->stderr_target != 0 ) {
                p = *pipes;
                while (p) {
                    if (p->id == (*cmd)->stderr_target) {
                        err = p->pipefd[1];
                        close_err = 0;
                        break;
                    }
                    p = p->next_pipe;
                }
                /* multi-line pipe */
                if (!p) return -1;
            } else {
                err = STDERR_FILENO;
                close_err = 1;
            }
        } else {
            err = STDERR_FILENO;
            out = *(pipefd+2*i+1);
            close_out = 1;
            close_err = 1;
        }

        if (!strcmp(c->cmd, "exit")) {
            exit(EXIT_SUCCESS);
        } else if (!strcmp(c->cmd, "printenv")) {
            snprintf(resp, 10240, "%s=%s\n", c->argv[1], getenv(c->argv[1])); 
            write(out, resp, strlen(resp));
            return 0;
        } else if (!strcmp(c->cmd, "setenv") ) {
            setenv(c->argv[1], c->argv[2], 1);
            return 0;
        }

        fork_pipes(c, in, out, err);
        wait(&status);
        if ((*cmd)->filename) {
            int fd = open(tempfile, O_RDONLY);
            unlink(tempfile);
            int filefd = open((*cmd)->filename, O_WRONLY|O_TRUNC|O_CREAT, 0644);
            int len=0;
            while ( (len=read(fd, resp, 1024)) >  0 ) {
                write(filefd, resp, len);
            }
            close(fd);
            close(filefd);
            free((*cmd)->filename);
        }
        //fprintf(stderr, "Status: %d\n", status);
        if (WIFEXITED(status) && WEXITSTATUS(status) == EXIT_FAILURE) {
            for (p = *pipes; p; p=p->next_pipe) {
                if (p->id >= line_id) {
                    ++(p->id);
                }
            }
        } else { 
            //fprintf(stderr, "Exit normally.\n");
            if (in != STDIN_FILENO) close(in);
            if (err != STDERR_FILENO && close_err) close(err);
            if (out != STDOUT_FILENO && close_out) close(out);
            del_pipe(pipes, line_id);
        }
        c=c->next_cmd;
    }

    free(pipefd);
    return 0;

    onerror:
    free(pipefd);
    return -1;
}

int fork_pipes(struct cmd* cmd, int in, int out, int err) {
    pid_t pid;

    //fprintf(stderr, "Cmd: %s, In: %d, Out: %d, Err: %d\n", cmd->cmd, in, out, err);

    pid = fork();

    if (pid > 0) {
        return 0;

    } else if (pid == 0) {
        if (in != STDIN_FILENO) dup2(in, STDIN_FILENO);
        if (in != STDOUT_FILENO) dup2(out, STDOUT_FILENO);
        if (in != STDERR_FILENO) dup2(err, STDERR_FILENO);
        execvp(cmd->cmd, cmd->argv); 
        fprintf(stderr, "Unknown command: [%s].\n", cmd->cmd);
        exit(EXIT_FAILURE);

    } else {
        perror("fork()");
        return -1;
    }
}

