#include "conn.h"
#include "misc.h"

int connectTCP(char *host, uint16_t port) {
    struct hostent h;
    struct hostent *result;
    int ret = 0;
    int h_error = 0;
    char buf[1024];
    struct sockaddr_in target;
    int msock = 0;

    host = trim(host);

    if (strlen(host) == 0) {
        fprintf(stderr, "Empty hostname is not allowed.\n");
        return -1;
    }

    bzero(&target, sizeof(target));

    target.sin_family = AF_INET;
    target.sin_port = htons(port);

    ret = gethostbyname_r(host, &h, buf, 1024, &result, &h_error);
    if (!result) {
        if (h_errno == HOST_NOT_FOUND) {
            fprintf(stderr, "Host %s not found.\n", host);
        } else if (h_errno == TRY_AGAIN) {
            fprintf(stderr, "Error with name server, please try again.\n");
        } else if (h_errno == NO_DATA) {
            fprintf(stderr, "The specifed host doesn't have a valid ip address.\n");
        }
    } else {
        /* copy the resolved ip to sockaddr_in struct */
        memcpy(&target.sin_addr, h.h_addr_list[0], h.h_length);
    }

    msock = socket(AF_INET, SOCK_STREAM, 0);
    if (msock < 0) {
        perror("socket()");
        return -1;
    }

    ret = connect(msock, (struct sockaddr*) &target, sizeof(target));
    if (ret < 0) {
        perror("connect()");
        return -1;
    }

    return msock;
}

int passiveTCP(char *host, uint16_t port, int backlog) {
    int msock = 0;
    struct sockaddr_in addr;
    struct hostent h;
    struct hostent *result;
    int ret = 0;
    int h_error = 0;
    char buf[1024];
    int opt = 1;
    
    bzero(&addr, sizeof(addr));
    msock = socket(AF_INET, SOCK_STREAM, 0);
    if (msock < 0) {
        perror("socket()");
        return -1;
    }

    /* set so_reuseaddr */
    ret = setsockopt(msock, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));
    if (ret < 0) {
        perror("setsockopt()");
        return -1;
    }

    host = trim(host);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);

    /* if host == "0.0.0.0" then INADDR_ANY is used */
    if (!strcmp("0.0.0.0", trim(host))) {
        addr.sin_addr.s_addr = INADDR_ANY;
    } else {
        ret = gethostbyname_r(host, &h, buf, 1024, &result, &h_error);
        if (!result) {
            if (h_errno == HOST_NOT_FOUND) {
                fprintf(stderr, "Host %s not found.\n", host);
            } else if (h_errno == TRY_AGAIN) {
                fprintf(stderr, "Error with name server, please try again.\n");
            } else if (h_errno == NO_DATA) {
                fprintf(stderr, "The specifed host doesn't have a valid ip address.\n");
            }
        } else {
            /* copy the resolved ip to sockaddr_in struct */
            memcpy(&addr.sin_addr, h.h_addr_list[0], h.h_length);
        }
    }

    ret = bind(msock, (struct sockaddr*) &addr, sizeof(addr));
    if (ret < 0) {
        perror("bind()");
        return -1;
    }

    ret = listen(msock, backlog);
    if (ret < 0) {
        perror("listen()");
        return -1;
    }

    return msock;
}

int sendall(int sock, void* buf, size_t len, int flags) {
    register int sent = 0;
    register int remaining = 0;
    register int ret = 0;

    remaining = len;

    while (remaining > 0) {
        ret = send(sock, buf+sent, remaining, flags);
        
        if (ret == -1) { 
            perror("send()");
            return -1;
        } else {
            sent += ret;
            remaining = len - sent;
        }
    }

    return sent;
}

