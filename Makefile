server: conn.o parser.o ras.c ras.h
	$(CC) $(CFLAGS) -o server parser.o conn.o misc.o ras.c

conn.o: misc.o conn.c conn.h
	$(CC) $(CFLAGS) -c -o conn.o misc.o conn.c

misc.o: misc.c misc.h
	$(CC) $(CFLAGS) -c -o misc.o misc.c

parse.o: misc.o parser.c parser.h
	$(CC) $(CFLAGS) -c -o parser.o misc.o parse.c

clean:
	rm -rf *.o *.out server ras/*.txt
